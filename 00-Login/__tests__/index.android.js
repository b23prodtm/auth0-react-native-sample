import 'react-native';
import React from 'react';
import Auth0Sample from '../index.android.js';
import {
  Button,
  View
} from 'react-native';
// Note: test renderer must be required after react-native.
import TestRenderer from 'react-test-renderer';

it('renders correctly', () => {
  const view = TestRenderer.create(
      <Auth0Sample />);
  expect(view.root.findByType(Button).props.title).toEqual('Log In');
});
